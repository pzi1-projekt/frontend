import App from './app';
import configureReduxStore from './redux/store';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';

const store = configureReduxStore(process.env.NODE_ENV === 'production' ? true : false);

ReactDOM.render(<Provider store = {store}><App/></Provider>, document.getElementById('root'));

reportWebVitals();
