import './style.scss';

import PropTypes from 'prop-types';
import React from 'react';

import { ArrowBack, CrossIcon } from '../../resources/icons';

const Button = ({
  onClick,
  className,
  style,
  variant,
  children
}) => {
  switch (variant) {
    case 'back': return <button className = {`universal-button small ${className || ''}`} style = {style}
      onClick = {onClick}>
      <ArrowBack/>
    </button>;

    case 'close': return <button className = {`universal-button small ${className || ''}`} style = {style}
      onClick = {onClick}>
      <CrossIcon/>
    </button>;

    default: return <button className = {`universal-button primary ${className || ''}`} style = {style}
      onClick = {onClick}>
      {children}
    </button>;
  }
};

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  variant: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string])
};

export default Button;
