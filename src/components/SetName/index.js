import './style.scss';

import { actionSocketSend } from '../../redux/actions/socket';
import { actionUiCloseSetName } from '../../redux/actions/ui';
import Button from '../Button';
import { ceUserSetName } from '../../helpers/socket/messages/user';
import { connect } from 'react-redux';
import Input from '../Input';
import PopupWrapper from '../PopupWrapper';
import PropTypes from 'prop-types';
import React from 'react';

const SetName = ({
  dispatch,
  isOpened,
  name
}) => {
  const [inputValue, setInputValue] = React.useState(name);

  const popupOnCloseHandler = React.useMemo(() => () => dispatch(actionUiCloseSetName()));

  const submitNameChange = React.useMemo(() => () => {
    if (inputValue && typeof inputValue === 'string' && inputValue.length <= 24) {
      dispatch(actionSocketSend(ceUserSetName(inputValue)));
      popupOnCloseHandler();
    }
  }, [inputValue]);

  React.useEffect(() => {
    setInputValue(name);
  }, [isOpened]);

  return <PopupWrapper opened = {isOpened} onClose = {popupOnCloseHandler}>
    <div className = 'set-name'>
      <h2>Choose your name <Button className = 'set-nameClose' variant = 'close'
        onClick = {popupOnCloseHandler}/></h2>
      <Input placeholder = {'Enter your name'} value = {inputValue}
        onChange = {setInputValue}/>
      <Button variant = 'primary' onClick = {submitNameChange}>Apply</Button>
    </div>
  </PopupWrapper>;
};

const mapStateToProps = state => ({
  isOpened: state.ui.setNameOpened,
  name: state.user.name || ''
});

SetName.propTypes = {
  dispatch: PropTypes.func,
  isOpened: PropTypes.bool,
  name: PropTypes.string
};

export default connect(mapStateToProps)(SetName);
