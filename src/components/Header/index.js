import './style.scss';

import { actionSocketSend } from '../../redux/actions/socket';
import { actionUiOpenSetName } from '../../redux/actions/ui';
import Button from '../Button';
import { ceChatClose } from '../../helpers/socket/messages/chat';
import { connect } from 'react-redux';
import NameDisplay from '../NameDisplay';
import PropTypes from 'prop-types';
import React from 'react';

import { AppLogo, PersonIcon } from '../../resources/icons';

const Header = ({
  inChatSocketKey,
  dispatch
}) => {
  const endChatClickHandler = React.useMemo(() => () => {
    dispatch(actionSocketSend(ceChatClose(inChatSocketKey)));
  }, [inChatSocketKey]);

  const userIconClickHandler = React.useMemo(() => () => dispatch(actionUiOpenSetName()), []);

  return <div className = 'header'>
    <div>
      {inChatSocketKey ? <Button variant = 'back' onClick = {endChatClickHandler}/> : null}
      <AppLogo/>
      <h1>SocketChat</h1>
    </div>
    <div>
      <NameDisplay/>
      <PersonIcon onClick = {userIconClickHandler}/>
    </div>
  </div>;
};

const mapStateToProps = state => ({
  inChatSocketKey: state.chat.chat.socketKey
});

Header.propTypes = {
  inChatSocketKey: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  dispatch: PropTypes.func
};

export default connect(mapStateToProps)(Header);
