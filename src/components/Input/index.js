import './style.scss';

import PropTypes from 'prop-types';
import React from 'react';

const Input = ({
  value,
  onChange,
  type,
  className,
  placeholder
}) => {
  const [inputValue, setValue] = React.useState(value);

  const onInputChange = React.useMemo(() => (e) => {
    if (onChange) {
      onChange(e.target.value);
    }
    setValue(e.target.value);
  }, [onChange]);

  return <input className = {`universal-input ${className || ''}`} placeholder = {placeholder}
    type = {type}
    value = {inputValue}
    onChange = {onInputChange}/>;
};

Input.propTypes = {
  value: PropTypes.any,
  onChange: PropTypes.func,
  type: PropTypes.string,
  className: PropTypes.string,
  placeholder: PropTypes.string
};

export default Input;
