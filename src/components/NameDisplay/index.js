import './style.scss';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

const NameDisplay = ({
  name
}) => <div className = 'name-display'>
  <span>Name:</span>
  <span>{name}</span>
</div>;

const mapStateToProps = state => ({
  name: state.user.name
});

NameDisplay.propTypes = {
  name: PropTypes.string
};

export default connect(mapStateToProps)(NameDisplay);
