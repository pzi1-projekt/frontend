import './style.scss';

import { actionResetAvailableUsers } from '../../redux/actions/user';
import { actionSocketSend } from '../../redux/actions/socket';
import { connect } from 'react-redux';
import { PersonIcon } from '../../resources/icons';
import PropTypes from 'prop-types';
import React from 'react';
import UserComponent from '../User/index';
import { useUserColors } from '../../helpers/hooks/user';

import { ceUserSubscribeList, ceUserUnsubscribeList } from '../../helpers/socket/messages/user';

const User = React.memo(UserComponent);

const UsersList = ({
  dispatch,
  availableUsers,
  hide
}) => {
  const colors = useUserColors(availableUsers);
  React.useEffect(() => {
    dispatch(actionSocketSend(ceUserSubscribeList()));
    return () => {
      dispatch(actionSocketSend(ceUserUnsubscribeList()));
      dispatch(actionResetAvailableUsers());
    };
  }, []);

  if (hide) {
    return null;
  }
  return <div className = 'users-list'>
    {availableUsers.map(user => <User color = {colors[user.socketKey]} key = {user.socketKey}
      name = {user.name}
      socketKey = {user.socketKey}/>)}
    {!availableUsers.length ? <h4 className = 'users-listEmpty'><PersonIcon/>No online users currently</h4> : null }
  </div>;
};

const mapStateToProps = state => ({
  availableUsers: state.user.listOfAvailable
});

UsersList.propTypes = {
  dispatch: PropTypes.func,
  availableUsers: PropTypes.array,
  hide: PropTypes.bool
};

export default connect(mapStateToProps)(UsersList);
