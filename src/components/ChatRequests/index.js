import './style.scss';

import ChatRequestComponent from '../ChatRequest';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

const ChatRequest = React.memo(ChatRequestComponent);

const ChatRequests = ({
  requests
}) => {
  const [requestsState, setRequestsState] = React.useState(requests);

  React.useEffect(() => {
    if (requests[requests.length - 1] && !requestsState.find(reqFromState => reqFromState.socketKey === requests[requests[requests.length - 1]?.socketKey])) {
      setRequestsState(requests.map(req => ({ ...req, opened: true })));
    } else {
      const newRequestState = [];
      const newRequestStateAfter = [];
      for (const req of requestsState) {
        if (requests.find(reqFromStore => reqFromStore.socketKey === req.socketKey)) {
          newRequestState.push(req);
          newRequestState.push(req);
        } else {
          newRequestState.push({ ...req, opened: false });
        }
      }
      setRequestsState(newRequestState);
      setTimeout(() => {
        setRequestsState(newRequestStateAfter);
      }, 250);
    }
  }, [requests]);

  return <div className = 'chat-requestsList'>
    {requestsState.map(req => <ChatRequest
      key = {req.socketKey}
      name = {req.name}
      opened = {req.opened}
      socketKey = {req.socketKey}
    />)}
  </div>;
};

const mapStateToProps = state => ({
  requests: state.chat.receivedRequests
});

ChatRequests.propTypes = {
  requests: PropTypes.array
};

export default connect(mapStateToProps)(ChatRequests);
