import './style.scss';

import PropTypes from 'prop-types';
import React from 'react';

const CenterWrapper = ({
  children,
  className,
  style
}) => <div className = {`center-wrapper ${className || ''}`} style = {style}>{children}</div>;

CenterWrapper.propTypes = {
  children: PropTypes.element,
  className: PropTypes.string,
  style: PropTypes.object
};

export default CenterWrapper;
