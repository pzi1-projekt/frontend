import './style.scss';

import { actionSocketSend } from '../../redux/actions/socket';
import { connect } from 'react-redux';
import Loader from '../Loader';
import { PersonIcon } from '../../resources/icons';
import PropTypes from 'prop-types';
import React from 'react';

import { actionAddNewSentRequest, actionRemoveSentRequest } from '../../redux/actions/chat';
import { ceChatAcceptRequest, ceChatRequest } from '../../helpers/socket/messages/chat';

const User = ({
  name,
  socketKey,
  color,
  isSentRequest,
  dispatch,
  isReceivedRequest
}) => {
  const userClickHandler = React.useMemo(() => () => {
    if (isReceivedRequest) {
      dispatch(actionSocketSend(ceChatAcceptRequest(socketKey)));
      return;
    }
    dispatch(actionSocketSend(ceChatRequest(socketKey)));
    dispatch(actionAddNewSentRequest(socketKey));

    setTimeout(() => {
      dispatch(actionRemoveSentRequest(socketKey));
    }, 20000);
  }, []);

  return <div className = 'user' style = {{ backgroundColor: color }}
    onClick = {userClickHandler}>
    {!isSentRequest ? <PersonIcon/> : <div className = 'userLoading'><Loader/>Request sent</div>}
    <h3>{name}</h3>
    <span>{socketKey}</span>
  </div>;
};

const mapStateToProps = (state, ownProps) => ({
  isSentRequest: state.chat.sentRequests.includes(ownProps.socketKey),
  isReceivedRequest: !!state.chat.receivedRequests.find(req => req.socketKey === ownProps.socketKey)
});

User.propTypes = {
  name: PropTypes.string,
  socketKey: PropTypes.string,
  color: PropTypes.string,
  isSentRequest: PropTypes.bool,
  dispatch: PropTypes.func,
  isReceivedRequest: PropTypes.bool
};

export default connect(mapStateToProps)(User);
