import './style.scss';

import { actionSocketSend } from '../../redux/actions/socket';
import { ceChatSend } from '../../helpers/socket/messages/chat';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';
import { SendIcon } from '../../resources/icons';

const MessageInput = ({
  disabled,
  dispatch
}) => {
  const [textValue, setTextValue] = React.useState('');
  const inputRef = React.useRef();

  const sendIconClickHandler = React.useMemo(() => () => {
    if (!disabled && textValue) {
      dispatch(actionSocketSend(ceChatSend(textValue)));
      setTextValue('');
      inputRef.current.focus();
    }
  }, [disabled, textValue]);

  const onTextareaChangeHandler = React.useMemo(() => (e) => {
    if (e.target.value && e.target.value.trim()) {
      setTextValue(e.target.value);
    } else {
      setTextValue('');
    }
  }, []);

  const onTextareaKeypressHandler = React.useMemo(() => (e) => {
    if (e.key === 'Enter' && !e.shiftKey && !e.altKey) {
      sendIconClickHandler();
      e.preventDefault();
    }
  }, [disabled, textValue]);

  return <div className = 'message-inputHolder'>
    <textarea
      className = 'message-input'
      ref = {inputRef}
      value = {textValue}
      onChange = {onTextareaChangeHandler}
      onKeyPress = {onTextareaKeypressHandler}
    />
    <SendIcon className = 'message-inputSend' onClick = {sendIconClickHandler}/>
  </div>;
};

const mapStateToProps = state => ({
  disabled: state.chat.chat.sending
});

MessageInput.propTypes = {
  disabled: PropTypes.bool,
  dispatch: PropTypes.func
};

export default connect(mapStateToProps)(MessageInput);
