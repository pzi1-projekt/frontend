import './style.scss';

import PropTypes from 'prop-types';
import React from 'react';

const PopupWrapper = ({
  children,
  onClose,
  opened
}) => {
  const [isOpened, setIsOpened] = React.useState(opened);
  const popupHolder = React.useRef();

  React.useEffect(() => {
    if (opened) {
      // Animate opening
      setIsOpened(true);
    }

    return () => {
      if (opened) {
        popupHolder.current.classList.add('animate-out');
        setTimeout(() => {
          setIsOpened(false);
        }, 250);
      }
    };
  }, [opened]);

  if (isOpened) {
    return <div className = 'popup-wrapper' ref = {popupHolder}>
      <div className = 'popup-wrapperBackground' onClick = {onClose}/>
      {children}
    </div>;
  }
  return null;
};

PopupWrapper.propTypes = {
  children: PropTypes.element,
  onClose: PropTypes.func,
  opened: PropTypes.bool
};

export default PopupWrapper;
