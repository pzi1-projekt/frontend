import './style.scss';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import React from 'react';

const Message = ({
  type,
  text,
  date,
  username
}) => <div className = {`messageHolder ${type}`}>
  <div className = 'message'>
    <span>{text}</span>
    <span>{date} {type === 'sent' ? '(You)' : `(${username})`}</span>
  </div>
</div>;

const mapStateToProps = state => ({
  username: state.user.listOfAvailable.find(user => user.socketKey === state.chat.chat.socketKey)?.name
});

Message.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string,
  date: PropTypes.string,
  socketKey: PropTypes.string,
  username: PropTypes.string
};

export default connect(mapStateToProps)(Message);
