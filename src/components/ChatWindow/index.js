import './style.scss';

import { connect } from 'react-redux';
import MessageComponent from '../Message';
import PropTypes from 'prop-types';
import React from 'react';

const Message = React.memo(MessageComponent);

const ChatWindow = ({
  messages
}) => {
  const chatWindowHolderRef = React.useRef();
  React.useEffect(() => {
    if (messages && messages[messages.length - 1]) {
      chatWindowHolderRef.current.scrollTop = chatWindowHolderRef.current.scrollHeight;
    }
  }, [messages]);
  return <div className = 'chat-windowHolder' ref = {chatWindowHolderRef}><div className = 'chat-window'>
    {messages.map((msg, index) => <Message
      date = {msg.date}
      key = {index}
      text = {msg.message}
      type = {msg.type}
    />)}
  </div></div>;
};

const mapStateToProps = (state, ownProps) => ({
  messages: state.chat.chat.messages
});

ChatWindow.propTypes = {
  messages: PropTypes.array
};

export default connect(mapStateToProps)(ChatWindow);
