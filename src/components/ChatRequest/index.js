import './style.scss';

import { actionDeleteReceivedRequest } from '../../redux/actions/chat';
import { actionSocketSend } from '../../redux/actions/socket';
import Button from '../Button';
import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch } from 'react-redux';

import { ceChatAcceptRequest, ceChatDeleteRequest } from '../../helpers/socket/messages/chat';

const ChatRequest = ({
  socketKey,
  name,
  opened
}) => {
  const dispatch = useDispatch();
  const buttonApproveClickHandler = React.useMemo(() => () => {
    dispatch(actionSocketSend(ceChatAcceptRequest(socketKey)));
    dispatch(actionDeleteReceivedRequest(socketKey));
  }, []);
  const buttonDeclineClickHandler = React.useMemo(() => () => {
    dispatch(actionSocketSend(ceChatDeleteRequest(socketKey)));
    dispatch(actionDeleteReceivedRequest(socketKey));
  }, []);

  return <div className = {`chat-request ${opened ? '' : 'closed'}`}>
    <div>
      <span>{socketKey}</span>
      <span>{name}</span>
    </div>
    <div>
      <Button onClick = {buttonApproveClickHandler}>Approve</Button>
      <Button onClick = {buttonDeclineClickHandler}>Decline</Button>
    </div>
  </div>;
};

ChatRequest.propTypes = {
  socketKey: PropTypes.string,
  name: PropTypes.string,
  opened: PropTypes.bool
};

export default ChatRequest;
