import './style.scss';

import PropTypes from 'prop-types';
import React from 'react';

const Loader = ({
  size
}) => <div className = {`loader ${size ? size : ''}`}/>;

Loader.propTypes = {
  size: PropTypes.string
};

export default Loader;
