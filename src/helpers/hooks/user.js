import React from 'react';
import { USER_COLORS } from '../ constants/colors';

export const useUserColors = (list) => {
  const [colors, setColors] = React.useState({});

  React.useEffect(() => {
    const newColors = {};
    list.forEach((user) => {
      if (!colors[user.socketKey]) {
        newColors[user.socketKey] = USER_COLORS[parseInt(Math.random() * 100, 10)];
      } else {
        newColors[user.socketKey] = colors[user.socketKey];
      }
    });
    setColors(newColors);
  }, [list]);

  return colors;
};
