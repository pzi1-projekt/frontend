import { CE_KEYS } from '../messageKeys';

export const ceChatRequest = socketKey => ({
  key: CE_KEYS.CHAT.REQUEST,
  data: {
    socketKey
  }
});

export const ceChatAcceptRequest = socketKey => ({
  key: CE_KEYS.CHAT.ACCEPT_REQUEST,
  data: {
    socketKey
  }
});

export const ceChatDeleteRequest = socketKey => ({
  key: CE_KEYS.CHAT.DELETE_REQUEST,
  data: {
    socketKey
  }
});

export const ceChatClose = socketKey => ({
  key: CE_KEYS.CHAT.CLOSE,
  data: {
    socketKey
  }
});

export const ceChatSend = message => ({
  key: CE_KEYS.CHAT.SEND_MESSAGE,
  data: {
    message
  }
});
