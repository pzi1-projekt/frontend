import { CE_KEYS } from '../messageKeys';

export const ceUserSetName = name => ({
  key: CE_KEYS.USER.SET_NAME,
  data: {
    name
  }
});

export const ceUserSubscribeList = () => ({
  key: CE_KEYS.USER.SUBSCRIBE_LIST,
  data: {}
});

export const ceUserUnsubscribeList = () => ({
  key: CE_KEYS.USER.UNSUBSCRIBE_LIST,
  data: {}
});
