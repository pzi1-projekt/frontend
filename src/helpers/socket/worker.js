let url = null;
let ws = null;

onmessage = function (evt) {
  if (evt.data.task) {
    switch (evt.data.task) {
      case 'connect':
        url = evt.data.url;
        socketConnect();
        break;

      case 'disconnect':
        ws.close();
        break;

      case 'send':
        ws.send(JSON.stringify(evt.data.payload));
        break;

      default:
        console.log(`${evt.data.task} : task not valid`);
        break;
    }
  }
};

// let reconnectCounter = 1;
// let waitingTime = 5000;
let keepAliveInterval = null;

function wsOnError() {
  console.log('socket error');
  postMessage({ type: 'socket', status: 'error', msg: 'need to reconnect' });
}

function wsOnMessage(evt) {
  postMessage({ type: 'protocol', response: JSON.parse(evt.data) });
}

function wsOnClose() {
  // console.log('socket close');
  postMessage({
    type: 'socket',
    status: 'close',
    response: {
      key: 'close'
    },
    payload: 'Connection Error!'
  });
  ws = null;

  if (keepAliveInterval) {
    clearInterval(keepAliveInterval);
    keepAliveInterval = null;
  }
}

function keepAliveHandler() {
  ws.send('keep alive');
}

function wsOnOpen() {
  // console.log('socket open');
  postMessage({
    type: 'socket',
    status: 'open',
    response: {
      key: 'connected'
    },
    event: '/connect'
  });
  if (!keepAliveInterval) {
    keepAliveInterval = setInterval(keepAliveHandler, 30000);
  }
}

function socketConnect() {
  ws = new WebSocket(url);
  ws.onerror = wsOnError;
  ws.onmessage = wsOnMessage;
  ws.onclose = wsOnClose;
  ws.onopen = wsOnOpen;
}
