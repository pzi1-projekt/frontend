export const checkApiStatusCorrect = payload => payload.status ? payload.status.code >= 200 && payload.status.code <= 299 : true;
