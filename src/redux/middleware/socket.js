import { checkApiStatusCorrect } from '../../helpers/socket/helpers';

import { actionAddNewReceivedMessage, actionAddNewReceivedRequest, actionAddNewSentMessage, actionCloseChat, actionDeleteReceivedRequest, actionNewMessageDelivered, actionNewMessageSent, actionRemoveSentRequest, actionStartChat } from '../actions/chat';
import { actionLoadAvailableUsers, actionSaveUser, actionUpdateUserName } from '../actions/user';
import { actionSocketClosed, actionSocketConnected } from '../actions/socket';
import { CE_KEYS, IE_KEYS, SE_KEYS } from '../../helpers/socket/messageKeys';
import { SOCKET_CONNECT, SOCKET_SEND } from '../actions/types';

const socketReduxMiddleware = () => {
  let socketWorker = null;

  const onWorkerMessage = store => (message) => {
    const { data: { event, response } } = message;
    const eventKey = event || response.key;
    switch (eventKey) {
      case '/connect':
        store.dispatch(actionSocketConnected());
        return;

      case 'close':
        store.dispatch(actionSocketClosed());
        return;

      case IE_KEYS.USER.INFORMATION:
        store.dispatch(actionSaveUser(response.data.name, response.data.socketKey));
        return;

      case SE_KEYS.USER.SET_NAME:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionUpdateUserName(response.data.name));
        }
        break;

      case IE_KEYS.USER.NAME_UPDATED:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionUpdateUserName(response.data.name, response.data.socketKey));
        }
        break;

      case SE_KEYS.USER.SUBSCRIBE_LIST:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionLoadAvailableUsers(response.data.users));
        }
        break;

      case SE_KEYS.CHAT.REQUEST:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionStartChat(response.data.socketKey));
        }
        // Remove sent chat request
        store.dispatch(actionRemoveSentRequest(response.data.socketKey));
        break;

      case SE_KEYS.CHAT.ACCEPT_REQUEST:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionStartChat(response.data.socketKey));
        }
        break;

      case IE_KEYS.CHAT.CLOSED:
        store.dispatch(actionCloseChat(response.data.socketKey));
        break;

      case SE_KEYS.CHAT.CLOSE:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionCloseChat(response.data.socketKey));
        }
        break;

      case IE_KEYS.CHAT.CHAT_REQUEST:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionAddNewReceivedRequest(response.data.socketKey, response.data.userName));
          setTimeout(() => {
            if (store.getState().chat.receivedRequests.find(req => req.socketKey === response.data.socketKey)) {
              store.dispatch(actionDeleteReceivedRequest(response.data.socketKey));
            }
          }, 20000);
        }
        break;

      case SE_KEYS.CHAT.SEND_MESSAGE:
        if (checkApiStatusCorrect(response)) {
          store.dispatch(actionAddNewSentMessage(response.data.message));
        }
        store.dispatch(actionNewMessageDelivered());
        break;

      case IE_KEYS.CHAT.MESSAGE_RECEIVED:
        store.dispatch(actionAddNewReceivedMessage(response.data.message));
        break;

      default: return;
    }
  };

  return store => next => (action) => {
    switch (action.type) {
      case SOCKET_CONNECT:
        socketWorker = new Worker(new URL('../../helpers/socket/worker.js', import.meta.url));
        socketWorker.postMessage({
          task: 'connect',
          url: action.url
        });
        socketWorker.onmessage = onWorkerMessage(store);
        next(action);
        break;

      case SOCKET_SEND:
        if (action.payload.key === CE_KEYS.CHAT.SEND_MESSAGE) {
          store.dispatch(actionNewMessageSent());
        }
        socketWorker.postMessage({
          task: 'send',
          payload: action.payload
        });
        break;

      default: next(action); break;
    }
  };
};

export default socketReduxMiddleware();
