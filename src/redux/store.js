import { createLogger } from 'redux-logger';
import rootReducer from './reducers/root';
import socketReduxMiddleware from './middleware/socket';

import { applyMiddleware, createStore } from 'redux';

const reduxLogger = createLogger({
  collapsed: true
});

const configureReduxStore = productive => createStore(rootReducer, productive ? applyMiddleware(socketReduxMiddleware) : applyMiddleware(socketReduxMiddleware, reduxLogger));

export default configureReduxStore;
