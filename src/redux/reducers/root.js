import chatReducer from './chat';
import { combineReducers } from 'redux';
import socketReducer from './socket';
import uiReducer from './ui';
import userReducer from './user';

const rootReducer = combineReducers({
  socket: socketReducer,
  user: userReducer,
  ui: uiReducer,
  chat: chatReducer
});

export default rootReducer;
