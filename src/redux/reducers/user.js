const { LOAD_AVAILABLE_USERS, RESET_AVAILABLE_USERS, SAVE_USER, UPDATE_AVAILABLE_USERS, UPDATE_USER_NAME } = require('../actions/types');

const initialState = {
  name: null,
  socketKey: null,
  listOfAvailable: [],
  subscribedToList: false
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_USER:
      return {
        ...state,
        name: action.name,
        socketKey: action.socketKey
      };

    case UPDATE_USER_NAME:
      if (!action.socketKey) {
        return {
          ...state,
          name: action.name
        };
      }
      return {
        ...state,
        listOfAvailable: state.listOfAvailable.map(user => user.socketKey === action.socketKey ? ({ name: action.name, socketKey: action.socketKey }) : user)
      };

    case LOAD_AVAILABLE_USERS:
      return {
        ...state,
        listOfAvailable: [...action.users],
        subscribedToList: true
      };

    case UPDATE_AVAILABLE_USERS:
      return {
        ...state,
        listOfAvailable: [...state.listOfAvailable, action.user]
      };

    case RESET_AVAILABLE_USERS:
      return {
        ...state,
        listOfAvailable: [],
        subscribedToList: false
      };

    default: return state;
  }
};

export default userReducer;
