const { SOCKET_CLOSED, SOCKET_CONNECT, SOCKET_CONNECTED, SOCKET_RECEIVED, SOCKET_SEND } = require('../actions/types');

const initialState = {
  connected: false,
  url: null,
  connecting: true
};

const socketReducer = (state = initialState, action) => {
  switch (action.type) {
    case SOCKET_CONNECT:
      return {
        ...state,
        connecting: true,
        url: action.url
      };

    case SOCKET_CONNECTED:
      return {
        ...state,
        connecting: false,
        connected: true
      };

    case SOCKET_CLOSED:
      return {
        ...state,
        connected: false
      };

    default: return state;
  }
};

export default socketReducer;
