import { CLOSE_SET_NAME, OPEN_SET_NAME } from '../actions/types';

const initialState = {
  setNameOpened: false
};

const uiReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_SET_NAME:
      return {
        ...state,
        setNameOpened: true
      };

    case CLOSE_SET_NAME:
      return {
        ...state,
        setNameOpened: false
      };

    default: return state;
  }
};

export default uiReducer;
