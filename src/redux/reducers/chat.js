import { ADD_NEW_RECEIVED_MESSAGE, ADD_NEW_RECEIVED_REQUEST, ADD_NEW_SENT_MESSAGE, ADD_NEW_SENT_REQUEST, CLOSE_CHAT, NEW_MESSAGE_DELIVERED, NEW_MESSAGE_SENT, REMOVE_RECEIVED_REQUEST, REMOVE_SENT_REQUEST, START_CHAT } from '../actions/types';

const initialState = {
  sentRequests: [],
  receivedRequests: [],
  chat: {
    messages: [],
    socketKey: null,
    sending: false
  }
};

const chatReducer = (state = initialState, action) => {
  let date = null;
  switch (action.type) {
    case NEW_MESSAGE_DELIVERED:
      return {
        ...state,
        chat: {
          ...state.chat,
          sending: false
        }
      };

    case NEW_MESSAGE_SENT:
      return {
        ...state,
        chat: {
          ...state.chat,
          sending: true
        }
      };

    case ADD_NEW_RECEIVED_REQUEST:
      return {
        ...state,
        receivedRequests: [...state.receivedRequests, { socketKey: action.socketKey, name: action.name }]
      };

    case REMOVE_RECEIVED_REQUEST:
      return {
        ...state,
        receivedRequests: state.receivedRequests.filter(req => req.socketKey !== action.socketKey)
      };

    case REMOVE_SENT_REQUEST:
      if (state.sentRequests.includes(action.socketKey)) {
        const copy = [...state.sentRequests];
        copy.splice(copy.indexOf(action.socketKey), 1);
        return {
          ...state,
          sentRequests: copy
        };
      }
      return state;

    case START_CHAT:
      return {
        ...state,
        chat: {
          socketKey: action.socketKey,
          messages: []
        }
      };

    case CLOSE_CHAT:
      return {
        ...state,
        chat: initialState.chat
      };

    case ADD_NEW_SENT_MESSAGE:
      date = new Date();
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...state.chat.messages, { type: 'sent', message: action.message, date: `${date.getHours()}:${date.getMinutes()}` }]
        }
      };

    case ADD_NEW_RECEIVED_MESSAGE:
      date = new Date();
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...state.chat.messages, { type: 'received', message: action.message, date: `${date.getHours()}:${date.getMinutes()}` }]
        }
      };

    case ADD_NEW_SENT_REQUEST:
      return {
        ...state,
        sentRequests: [...state.sentRequests, action.socketKey]
      };

    default: return state;
  }
};

export default chatReducer;
