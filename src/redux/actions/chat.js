import { ADD_NEW_RECEIVED_MESSAGE, ADD_NEW_RECEIVED_REQUEST, ADD_NEW_SENT_MESSAGE, ADD_NEW_SENT_REQUEST, CLOSE_CHAT, NEW_MESSAGE_DELIVERED, NEW_MESSAGE_SENT, REMOVE_RECEIVED_REQUEST, REMOVE_SENT_REQUEST, START_CHAT } from './types';

export const actionAddNewSentRequest = socketKey => ({
  type: ADD_NEW_SENT_REQUEST,
  socketKey
});

export const actionRemoveSentRequest = socketKey => ({
  type: REMOVE_SENT_REQUEST,
  socketKey
});

export const actionAddNewReceivedRequest = (socketKey, name) => ({
  type: ADD_NEW_RECEIVED_REQUEST,
  socketKey,
  name
});
export const actionDeleteReceivedRequest = socketKey => ({
  type: REMOVE_RECEIVED_REQUEST,
  socketKey
});
export const actionStartChat = socketKey => ({
  type: START_CHAT,
  socketKey
});
export const actionCloseChat = () => ({
  type: CLOSE_CHAT
});
export const actionAddNewReceivedMessage = message => ({
  type: ADD_NEW_RECEIVED_MESSAGE,
  message
});
export const actionAddNewSentMessage = message => ({
  type: ADD_NEW_SENT_MESSAGE,
  message
});
export const actionNewMessageSent = () => ({
  type: NEW_MESSAGE_SENT
});
export const actionNewMessageDelivered = () => ({
  type: NEW_MESSAGE_DELIVERED
});
