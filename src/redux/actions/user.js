import { LOAD_AVAILABLE_USERS, RESET_AVAILABLE_USERS, SAVE_USER, UPDATE_AVAILABLE_USERS, UPDATE_USER_NAME } from './types';

export const actionSaveUser = (name, socketKey) => ({
  type: SAVE_USER,
  name,
  socketKey
});

export const actionUpdateUserName = (name, socketKey = false) => ({
  type: UPDATE_USER_NAME,
  name,
  socketKey
});

export const actionUpdateAvailableUsers = user => ({
  type: UPDATE_AVAILABLE_USERS,
  user
});

export const actionLoadAvailableUsers = users => ({
  type: LOAD_AVAILABLE_USERS,
  users
});

export const actionResetAvailableUsers = () => ({
  type: RESET_AVAILABLE_USERS
});
