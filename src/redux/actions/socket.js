import { SOCKET_CLOSE, SOCKET_CLOSED, SOCKET_CONNECT, SOCKET_CONNECTED, SOCKET_RECEIVED, SOCKET_SEND } from './types';

export const actionSocketConnect = url => ({
  type: SOCKET_CONNECT,
  url
});

export const actionSocketSend = payload => ({
  type: SOCKET_SEND,
  payload
});

export const actionSocketReceived = message => ({
  type: SOCKET_RECEIVED,
  message
});

export const actionSocketClose = () => ({
  type: SOCKET_CLOSE
});

export const actionSocketConnected = () => ({
  type: SOCKET_CONNECTED
});

export const actionSocketClosed = () => ({
  type: SOCKET_CLOSED
});
