import { CLOSE_SET_NAME, OPEN_SET_NAME } from './types';

export const actionUiOpenSetName = () => ({
  type: OPEN_SET_NAME
});

export const actionUiCloseSetName = () => ({
  type: CLOSE_SET_NAME
});
