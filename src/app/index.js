import '../resources/style/root.scss';

import { actionSocketConnect } from '../redux/actions/socket';
import CenterWrapper from '../components/CenterWrapper';
import ChatRequestsComponent from '../components/ChatRequests';
import ChatWindow from '../components/ChatWindow';
import { connect } from 'react-redux';
import HeaderComponent from '../components/Header';
import Loader from '../components/Loader';
import MessageInput from '../components/MessageInput';
import PropTypes from 'prop-types';
import React from 'react';
import SetNameComponent from '../components/SetName';
import UsersList from '../components/UsersList';

const Header = React.memo(HeaderComponent);
const ChatRequests = React.memo(ChatRequestsComponent);
const SetName = React.memo(SetNameComponent);

const App = ({ dispatch, socketConnected, socketConnecting, inChat }) => {
  React.useEffect(() => {
    document.documentElement.style.setProperty('--vh', `${window.innerHeight}px`);
    // Add event listener on resize to update viewport height
    window.addEventListener('resize', () => {
      setTimeout(() => {
        document.documentElement.style.setProperty('--vh', `${window.innerHeight}px`);
      }, 0);
    });
    dispatch(actionSocketConnect('wss://socket-chat-api.ivan-pl.com'));
  }, []);

  if (socketConnecting) {
    return <CenterWrapper style = {{ height: '100vh' }}><Loader/></CenterWrapper>;
  } else if (socketConnected) {
    return <>
      <Header/>
      <UsersList hide = {inChat}/>
      {inChat ? <>
        <ChatWindow/>
        <MessageInput/>
      </>
        : null
      }
      <SetName/>
      <ChatRequests/>
    </>;
  }
  return <CenterWrapper style = {{ height: '100vh' }}><Loader/></CenterWrapper>;
};

const mapStateToProps = state => ({
  socketConnecting: state.socket.connecting,
  socketConnected: state.socket.connected,
  inChat: !!state.chat.chat.socketKey
});

App.propTypes = {
  inChat: PropTypes.bool,
  dispatch: PropTypes.func,
  socketConnecting: PropTypes.bool,
  socketConnected: PropTypes.bool
};

export default connect(mapStateToProps)(App);
